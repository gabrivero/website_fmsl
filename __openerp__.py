{
    # Theme information
    'name': "Website FMSL Theme",
    'description': """
    Módulo Odoo website para el Festival Misionero de SL 2014
    """,
    'category': 'Theme',
    'version': '1.0',
    'depends': ['website'],

    # templates, pages, and snippets
    'data': [
        'views/themes.xml',
        'data/demo.xml',
        #'views/options.xml',
        #'views/snippets.xml',
    ],
    'demo': [
        
    ],

    # Your information
    'author': "Gabriela Rivero",
    'website': "",
}